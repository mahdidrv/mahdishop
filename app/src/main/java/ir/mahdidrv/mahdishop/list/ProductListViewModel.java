package ir.mahdidrv.mahdishop.list;

import java.util.List;

import io.reactivex.Single;
import ir.mahdidrv.mahdishop.model.Products;
import ir.mahdidrv.mahdishop.model.api.ApiService;
import ir.mahdidrv.mahdishop.providers.ApiServiceProvider;

public class ProductListViewModel {
    private ApiService apiService = ApiServiceProvider.serviceProvider();


    public Single<List<Products>> products (int sortType){
     return apiService.getProducts(sortType);
    }
}
