package ir.mahdidrv.mahdishop.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import ir.mahdidrv.mahdishop.R;

public class SortAdapter extends RecyclerView.Adapter<SortAdapter.SortViewHolder> {

    private String [] sortTYpeIds = new String[]{
            "جدیدترین",
            "پر بازدیدترین",
            "قیمت از زیاد به کم",
            "قیمت از کم به زیاد"
    };

    private Context context;
    private int selectedSortType;
    private OnSortClickListener onSortClickListener;

    public SortAdapter (Context context, int selectedSortType, OnSortClickListener onSortClickListener){
        this.context = context;
        this.selectedSortType = selectedSortType;
        this.onSortClickListener = onSortClickListener;
    }

    @NonNull
    @Override
    public SortViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SortViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sort_chips,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final SortViewHolder holder, int position) {
        holder.titleTv.setText(sortTYpeIds[position]);

        if(position == selectedSortType){
            holder.titleTv.setBackgroundResource(R.drawable.shape_chips_selected);
            holder.titleTv.setTextColor(ContextCompat.getColor(context,R.color.white));
        } else {
            holder.titleTv.setBackgroundResource(R.drawable.shape_chips_unselected);
            holder.titleTv.setTextColor(ContextCompat.getColor(context,R.color.colorAccent));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.getAdapterPosition() != selectedSortType)
                onSortClickListener.onClick(holder.getAdapterPosition());
                // change last selected item to unSelect item
                notifyItemChanged(selectedSortType);
                selectedSortType = holder.getAdapterPosition();
                /*when item build and insert in list OnBindView call. but onClick change item background and
                color when item created! and change color can't call
                we can change color with notify because notify Force to call OnBind method :) */
                notifyItemChanged(selectedSortType);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sortTYpeIds.length;
    }

    public class SortViewHolder extends RecyclerView.ViewHolder{

        private TextView titleTv;
        public SortViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTv = (TextView) itemView;
        }
    }

    // when click a sort we tell activity to Send a new Request?
    public interface OnSortClickListener {
        void onClick(int sortType);
    }
}
