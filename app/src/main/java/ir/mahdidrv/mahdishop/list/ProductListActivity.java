package ir.mahdidrv.mahdishop.list;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.mahdidrv.mahdishop.BaseActivity;
import ir.mahdidrv.mahdishop.R;
import ir.mahdidrv.mahdishop.main.ProductAdapter;
import ir.mahdidrv.mahdishop.model.Products;
import ir.mahdidrv.mahdishop.model.api.CustomSingleObserver;

public class ProductListActivity extends BaseActivity {

    public static final String EXTRA_KEY_SORT = "sort";
    private int sortType;
    private ProductListViewModel viewModel = new ProductListViewModel();
    private CompositeDisposable compositeDisposable;

    private RecyclerView productListRv;
    private RecyclerView sortRv;

    private SortAdapter sortAdapter;
    private ProductAdapter productAdapter;


    private CustomSingleObserver<List<Products>> productObserver = new CustomSingleObserver<List<Products>>(this) {
        @Override
        public void onSubscribe(Disposable d) {
            /*if (d != null) {
                compositeDisposable.add(d);
            }*/
        }

        @Override
        public void onSuccess(List<Products> products) {
            productAdapter.setProducts(products);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        // get sortType from last Activity (Main Activity)
        // second Arg has a Default sortType
        sortType = getIntent().getIntExtra(EXTRA_KEY_SORT, Products.SORT_LATEST);

        setupView();
        observ();
    }

    private void observ() {

        viewModel.products(sortType)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(productObserver);
    }

    private void setupView() {
        productListRv = findViewById(R.id.rv_list_products);
        productListRv.setLayoutManager(new GridLayoutManager(this,2));
        productAdapter = new ProductAdapter();
        productListRv.setAdapter(productAdapter);

        sortRv = findViewById(R.id.rv_list_sort);
        sortRv.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,true));
        sortAdapter = new SortAdapter(this, sortType, new SortAdapter.OnSortClickListener() {
            @Override
            public void onClick(int sortType) {
                // we can't use this, because in interface this mention OnClickMethod
                ProductListActivity.this.sortType = sortType;
                observ();
            }
        });
        sortRv.setAdapter(sortAdapter);

        ImageView backBtn = findViewById(R.id.iv_list_back);
        backBtn.setOnClickListener(v -> finish());


    }

    @Override
    public int getCoordinatorLayoutId() {
        return 0;
    }
}
