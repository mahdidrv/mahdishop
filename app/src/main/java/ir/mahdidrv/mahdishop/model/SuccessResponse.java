package ir.mahdidrv.mahdishop.model;

public class SuccessResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
