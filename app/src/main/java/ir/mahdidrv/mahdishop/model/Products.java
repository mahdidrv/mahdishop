package ir.mahdidrv.mahdishop.model;

import java.io.Serializable;

public class Products implements Serializable {

    public static final int SORT_LATEST=0;
    public static final int SORT_POPULAR=1;
    public static final int SORT_HIGHTOLOW=2;
    public static final int SORT_LOWTOHIGHT=3;

    private String image;
    private int price;
    private int discount;
    private int id;
    private String title;
    private int previous_price;
    private int status;

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return this.discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrevious_price() {
        return this.previous_price;
    }

    public void setPrevious_price(int previous_price) {
        this.previous_price = previous_price;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
