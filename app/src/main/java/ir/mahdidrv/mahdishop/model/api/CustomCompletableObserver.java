package ir.mahdidrv.mahdishop.model.api;

import android.util.Log;

import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import ir.mahdidrv.mahdishop.BaseActivity;
import ir.mahdidrv.mahdishop.exception.ExceptionMassageFactory;
import retrofit2.HttpException;

public abstract class CustomCompletableObserver implements CompletableObserver {
    private BaseActivity activity;

    public CustomCompletableObserver(BaseActivity activity){
        this.activity = activity;
    }

    @Override
    public void onError(Throwable e) {
        Log.e("test", "onError: customobserver " + e.getMessage() );
        activity.showSnackbarMessage(ExceptionMassageFactory.getMessage(e));
        if (e instanceof HttpException){
            //Action: for example open a Fragment (Register) for session
        }
    }
}
