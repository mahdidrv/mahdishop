package ir.mahdidrv.mahdishop.model;

public class Banners {
    private int link_type;
    private String image;
    private int id;
    private String link_value;

    public int getLink_type() {
        return this.link_type;
    }

    public void setLink_type(int link_type) {
        this.link_type = link_type;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink_value() {
        return this.link_value;
    }

    public void setLink_value(String link_value) {
        this.link_value = link_value;
    }
}
