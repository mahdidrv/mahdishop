package ir.mahdidrv.mahdishop.model.api;

import java.util.List;

import io.reactivex.Single;
import ir.mahdidrv.mahdishop.model.Banners;
import ir.mahdidrv.mahdishop.model.Products;
import ir.mahdidrv.mahdishop.model.SuccessResponse;
import ir.mahdidrv.mahdishop.model.Token;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @GET("product/list")
    Single<List<Products>> getProducts(
        @Query("sort") int mode);


    @GET("banner/slider")
    Single<List<Banners>> getBanners();

    @FormUrlEncoded
    @POST("auth/token")
    Single<Token> getToken(@Field("grant_type") String grantType,
                           @Field("client_id") Integer clientId,
                           @Field("client_secret") String clientSecret,
                           @Field("username") String username,
                           @Field("password") String password);

    @FormUrlEncoded
    @POST("user/register")
    Single<SuccessResponse> registerUser(@Field("email") String user,
                                         @Field("password") String password);


}
