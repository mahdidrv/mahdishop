package ir.mahdidrv.mahdishop.providers;


import ir.mahdidrv.mahdishop.model.api.ApiService;
import ir.mahdidrv.mahdishop.model.api.RetrofitSingleton;

public class ApiServiceProvider {

    private static ApiService apiService;
    public static ApiService serviceProvider(){
        if (apiService == null){
            apiService = RetrofitSingleton.getInstance().create(ApiService.class);
        }
        return apiService;
    }
}
