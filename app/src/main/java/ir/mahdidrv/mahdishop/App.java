package ir.mahdidrv.mahdishop;

import android.app.Application;

import ir.mahdidrv.mahdishop.auth.TokenContainer;
import ir.mahdidrv.mahdishop.auth.UserInfoManager;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TokenContainer.updateToken(new UserInfoManager(this).token());
    }
}
