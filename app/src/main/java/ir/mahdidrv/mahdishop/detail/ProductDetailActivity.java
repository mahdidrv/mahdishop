package ir.mahdidrv.mahdishop.detail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import ir.mahdidrv.mahdishop.R;
import ir.mahdidrv.mahdishop.model.Products;
import ir.mahdidrv.mahdishop.utils.PriceConverter;

public class ProductDetailActivity extends AppCompatActivity {


    public static String PRODUCT_EXTRA_KEY = "product_key";

    private TextView titleTv;
    private TextView prevPriceTv;
    private TextView correntPriceTv;
    private ImageView productCoverIv;
    private ImageView backIv;
    private Button addToCardBtn;

    private Products product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        product = (Products) getIntent().getSerializableExtra(PRODUCT_EXTRA_KEY);
        if (product == null){
            finish();
        }

        setupView();
    }

    private void setupView() {
        titleTv = findViewById(R.id.txt_detail_title);
        titleTv.setText(product.getTitle());

        productCoverIv = findViewById(R.id.iv_detail_productCover);
        Picasso.get().load(product.getImage()).into(productCoverIv);


        prevPriceTv = findViewById(R.id.txt_detail_prevPrice);
        prevPriceTv.setPaintFlags(prevPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        prevPriceTv.setText(String.valueOf(product.getPrevious_price()));

        correntPriceTv = findViewById(R.id.txt_detail_correntPrice);
        correntPriceTv.setText(PriceConverter.convert(product.getPrice()));


        backIv = findViewById(R.id.iv_detail_back);
        backIv.setOnClickListener(v -> finish());

        addToCardBtn = findViewById(R.id.btn_detail_addToCard);
        String message ="به سبد خرید اضافه شد :) ";
        addToCardBtn.setOnClickListener(v-> Snackbar.make(addToCardBtn,message,Snackbar.LENGTH_LONG).show());



    }
}
