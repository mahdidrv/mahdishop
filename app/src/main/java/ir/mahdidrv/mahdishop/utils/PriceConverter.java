package ir.mahdidrv.mahdishop.utils;

import ir.mahdidrv.mahdishop.R;

public class PriceConverter {

    public static String convert(long price){
        return String.valueOf(price) + " تومان";
    }
}
