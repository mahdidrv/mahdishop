package ir.mahdidrv.mahdishop.auth;

import android.content.Context;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;
import ir.mahdidrv.mahdishop.model.Token;
import ir.mahdidrv.mahdishop.model.api.ApiService;
import ir.mahdidrv.mahdishop.providers.ApiServiceProvider;

public class AuthenticationViewModel {

    private ApiService apiService = ApiServiceProvider.serviceProvider();
    private BehaviorSubject<Boolean> isLoginMode = BehaviorSubject.create();
    private BehaviorSubject<Boolean> progressVisibility = BehaviorSubject.create();


    public Completable authenticate(Context context, String username, String password) {
        Single<Token> singleToken;
        progressVisibility.onNext(true);
        String clientSecret = "kyj1c9sVcksqGU4scMX7nLDalkjp2WoqQEf8PKAC";
        if (isLoginMode.getValue() == null || isLoginMode.getValue()) {
            singleToken = apiService.getToken("password", 2, clientSecret,
                    username, password);
        } else {
            singleToken = apiService.registerUser(username, password)
                    .flatMap(successResponse -> apiService.getToken("password", 2, clientSecret,
                            username, password));
        }
        final UserInfoManager userInfoManager = new UserInfoManager(context);
        return singleToken.doOnSuccess(token -> {
            userInfoManager.saveToken(token.getAccessToken(), token.getRefreshToken());
            TokenContainer.updateToken(token.getAccessToken());
        })
                .doOnEvent((token, throwable) -> progressVisibility.onNext(false))
                .ignoreElement();

    }

    public void onChangeAuthenticationClick(){
        isLoginMode.onNext(isLoginMode.getValue() == null ? true: !isLoginMode.getValue());
    }

    public BehaviorSubject<Boolean> getIsLoginMode() {
        return isLoginMode;
    }

    public BehaviorSubject<Boolean> getProgressVisibility() {
        return progressVisibility;
    }
}
