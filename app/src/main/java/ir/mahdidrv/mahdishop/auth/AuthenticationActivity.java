package ir.mahdidrv.mahdishop.auth;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.mahdidrv.mahdishop.BaseActivity;
import ir.mahdidrv.mahdishop.R;
import ir.mahdidrv.mahdishop.model.api.CustomCompletableObserver;
import ir.mahdidrv.mahdishop.utils.KeyboardUtil;

public class AuthenticationActivity extends BaseActivity {

    private EditText userEt;
    private EditText passEt;
    private Button authBtn;
    private ImageView backIv;
    private TextView changeAuthMode;
    private LinearLayout progressBar;

    private AuthenticationViewModel viewModel;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        viewModel = new AuthenticationViewModel();
        setupView();
        observ();


    }

    private void observ() {
        Disposable isLoginModeDisposable = viewModel.getIsLoginMode().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isLoginMode -> {
                    if (isLoginMode) {
                        authBtn.setText("ورود به حساب کاربری");
                        changeAuthMode.setText("ثبت نام");
                    } else {
                        authBtn.setText("ثبت نام");
                        changeAuthMode.setText("ورود به حساب کاربری");
                    }
                });


        Disposable progressDisposable = viewModel.getProgressVisibility().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(shouldShowProgressBar -> progressBar.setVisibility(shouldShowProgressBar ? View.VISIBLE : View.GONE));

        compositeDisposable.add(isLoginModeDisposable);
        compositeDisposable.add(progressDisposable);


    }

    private void setupView() {


        userEt = findViewById(R.id.et_auth_username);
        passEt = findViewById(R.id.et_auth_password);
        authBtn = findViewById(R.id.btn_auth_sign);
        changeAuthMode = findViewById(R.id.tv_auth_changeLoginMode);
        progressBar = findViewById(R.id.ll_auth_progress);
        backIv = findViewById(R.id.iv_auth_back);

        changeAuthMode.setOnClickListener(v -> viewModel.onChangeAuthenticationClick());
        authBtn.setOnClickListener(v -> {
            KeyboardUtil.closeKeyboard(getCurrentFocus());
            viewModel.authenticate(AuthenticationActivity.this, userEt.getText().toString(), passEt.getText().toString())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new CustomCompletableObserver(this) {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onComplete() {
                            Toast.makeText(AuthenticationActivity.this, "خوش آمدید",Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
        });
        backIv.setOnClickListener(v -> this.finish());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Override
    public int getCoordinatorLayoutId() {
        return R.id.rl_auth_root;
    }
}
