package ir.mahdidrv.mahdishop.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.mahdidrv.mahdishop.AboutDialogFragment;
import ir.mahdidrv.mahdishop.BaseActivity;
import ir.mahdidrv.mahdishop.R;
import ir.mahdidrv.mahdishop.auth.AuthenticationActivity;
import ir.mahdidrv.mahdishop.auth.TokenContainer;
import ir.mahdidrv.mahdishop.auth.UserInfoManager;
import ir.mahdidrv.mahdishop.list.ProductListActivity;
import ir.mahdidrv.mahdishop.model.Banners;
import ir.mahdidrv.mahdishop.model.Products;
import ir.mahdidrv.mahdishop.model.api.CustomSingleObserver;

public class MainActivity extends BaseActivity implements View.OnClickListener {


    private static final Integer ITEM_CARD = 1;
    private static final Integer ITEM_AUTH = 2;
    private static final Integer ITEM_ABOUT = 3;
    private static final Integer ITEM_CONTACT = 4;

    private MainViewModel viewModel;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private RecyclerView latestProduct;
    private RecyclerView popularProduct;
    private ProductAdapter latestProductAdapter;
    private ProductAdapter popularProductAdapter;
    private ImageView bannerIv;
    private ImageView cartIv;
    private ImageView menuIv;

    private Drawer drawer;
    private UserInfoManager userInfoManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userInfoManager = new UserInfoManager(this);
        viewModel = new MainViewModel();
        setupView();
        setupDrawer();
        observe();

    }

    private void setupDrawer(){

        AccountHeader accountHeader = new AccountHeaderBuilder()
                .addProfiles(
                        new ProfileDrawerItem()
                        .withName("کاربر مهمان")
                )
                .withActivity(this)
                .withHeaderBackground(ContextCompat.getDrawable(MainActivity.this,R.color.primary))
                .build();



        SecondaryDrawerItem cardItemDrawer = new SecondaryDrawerItem()
                .withName("سبد خرید")
                .withIdentifier(ITEM_CARD)
                .withSelectable(false)
                .withIcon(ContextCompat.getDrawable(this,R.drawable.ic_shopping_cart_gray_24dp));


        String title;
        if(TokenContainer.getToken() != null) {
            title = "خروج از حساب کاربری";
        } else{
            title = "ورود یا ثبت نام";
        }
        SecondaryDrawerItem authItemDrawer = new SecondaryDrawerItem()
                .withName(title)
                .withSelectable(false)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if(TokenContainer.getToken() != null) {
                            userInfoManager.clear();
                            TokenContainer.updateToken(null);
                           // Toast.makeText(MainActivity.this,"از حساب کاربری خارج شدید", Toast.LENGTH_LONG).show();
                            String login = "ورود دوباره";
                            Snackbar snackbar = Snackbar
                                    .make(cartIv,"از حساب کاربری خارج شدید", Snackbar.LENGTH_INDEFINITE)
                                    .setAction(login, v -> startActivity(new Intent(MainActivity.this,AuthenticationActivity.class)));
                            snackbar.show();
                            setupDrawer();
                        } else {
                            startActivity(new Intent(MainActivity.this,AuthenticationActivity.class));
                        }
                        return false;
                    }
                })
                .withIdentifier(ITEM_AUTH)
                .withIcon(ContextCompat.getDrawable(this,R.drawable.ic_login_white));

        SecondaryDrawerItem aboutItemDrawer = new SecondaryDrawerItem()
                .withName("درباره ما")
                .withSelectable(false)
                .withIdentifier(ITEM_ABOUT)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        AboutDialogFragment sampleDialog = new AboutDialogFragment();
                        sampleDialog.show(getSupportFragmentManager(),null);
                        return false;
                    }
                })
                .withIcon(ContextCompat.getDrawable(this,R.drawable.ic_about_white));

        SecondaryDrawerItem contactItemDrawer = new SecondaryDrawerItem()
                .withName("ارتباط")
                .withSelectable(false)
                .withIdentifier(ITEM_CONTACT)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://mahdidrv.ir")));
                        return false;
                    }
                })
                .withIcon(ContextCompat.getDrawable(this,R.drawable.ic_contact_white));


        drawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(accountHeader)
                .withSelectedItem(-1)
                .addDrawerItems(cardItemDrawer,authItemDrawer,aboutItemDrawer,contactItemDrawer)
                .withDrawerGravity(Gravity.RIGHT)
                .build();


    }

    private void observe() {
        viewModel.latestProduct()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CustomSingleObserver<List<Products>>(this) {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Products> products) {
                        products.addAll(products);
                        latestProductAdapter.setProducts(products);

                    }
                });

        viewModel.popularProduct()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CustomSingleObserver<List<Products>>(this) {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Products> products) {
                        products.addAll(products);
                        popularProductAdapter.setProducts(products);

                    }
                });

        viewModel.banner()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CustomSingleObserver<List<Banners>>(this) {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Banners> banners) {
                        Log.i("test", "onSuccess: " + banners.get(0).getImage() + "  //  " + banners.get(0).getLink_value());
                        Picasso.get().load(banners.get(0).getImage()).into(bannerIv);
                    }
                });
    }

    private void setupView() {
        latestProduct = findViewById(R.id.rv_main_latestProducts);
        popularProduct = findViewById(R.id.rv_main_popularProducts);
        bannerIv = findViewById(R.id.iv_main_slider);
        menuIv = findViewById(R.id.iv_main_menu);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                this, RecyclerView.HORIZONTAL, true);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(
                this, RecyclerView.HORIZONTAL, true);


        latestProduct.setLayoutManager(linearLayoutManager);
        popularProduct.setLayoutManager(linearLayoutManager1);


        latestProductAdapter = new ProductAdapter();
        popularProductAdapter = new ProductAdapter();
        latestProduct.setAdapter(latestProductAdapter);
        popularProduct.setAdapter(popularProductAdapter);

        cartIv = findViewById(R.id.iv_main_cart);
        cartIv.setOnClickListener(v ->
            startActivity(new Intent(MainActivity.this, AuthenticationActivity.class)));


        menuIv.setOnClickListener(v -> drawer.openDrawer());


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Override
    public int getCoordinatorLayoutId() {
        return R.id.coordinator_main;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this,ProductListActivity.class);
        switch (v.getId()){
            case R.id.tv_main_viewwAllLatestProducts:
                intent.putExtra(ProductListActivity.EXTRA_KEY_SORT,Products.SORT_LATEST);
                break;
            case R.id.tv_main_viewwAllPopularProducts:
                intent.putExtra(ProductListActivity.EXTRA_KEY_SORT,Products.SORT_POPULAR);
                break;

                default:
        }
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupDrawer();
    }
}
