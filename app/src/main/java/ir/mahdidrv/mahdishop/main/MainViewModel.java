package ir.mahdidrv.mahdishop.main;

import java.util.List;

import io.reactivex.Single;
import ir.mahdidrv.mahdishop.model.Banners;
import ir.mahdidrv.mahdishop.model.Products;
import ir.mahdidrv.mahdishop.model.api.ApiService;
import ir.mahdidrv.mahdishop.providers.ApiServiceProvider;

public class MainViewModel {
    private ApiService apiService = ApiServiceProvider.serviceProvider();

    public Single<List<Products>> latestProduct(){
        return apiService.getProducts(Products.SORT_LATEST);
    }

    public Single<List<Products>> popularProduct(){
        return apiService.getProducts(Products.SORT_POPULAR);
    }

    public Single<List<Banners>> banner(){
        return apiService.getBanners();
    }
}
