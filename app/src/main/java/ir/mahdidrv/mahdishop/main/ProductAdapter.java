package ir.mahdidrv.mahdishop.main;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ir.mahdidrv.mahdishop.R;
import ir.mahdidrv.mahdishop.detail.ProductDetailActivity;
import ir.mahdidrv.mahdishop.model.Products;
import ir.mahdidrv.mahdishop.utils.PriceConverter;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewholder> {

    private List<Products> products = new ArrayList<>();

    public ProductAdapter() {

    }

    public void setProducts(List<Products> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ProductViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewholder holder, int position) {
        holder.bindProduct(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ProductViewholder extends RecyclerView.ViewHolder{


        ImageView productIv;
        TextView productTitleIv;
        TextView productPriceTv;
        TextView productPrevPriceTv;

        public ProductViewholder(@NonNull View item) {
            super(item);

            productIv = item.findViewById(R.id.iv_product_cover);
            productPrevPriceTv = item.findViewById(R.id.tv_product_prevPrice);
            productTitleIv = item.findViewById(R.id.tv_product_title);
            productPriceTv = item.findViewById(R.id.tv_product_price);

        }

        public void bindProduct(Products products){
            Picasso.get().load(products.getImage()).into(productIv);
            productPrevPriceTv.setText(products.getPrevious_price()+"");
            productPrevPriceTv.setPaintFlags(productPrevPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            productPriceTv.setText(PriceConverter.convert(products.getPrice()));
            productTitleIv.setText(products.getTitle());



            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), ProductDetailActivity.class);
                intent.putExtra(ProductDetailActivity.PRODUCT_EXTRA_KEY, products);

                v.getContext().startActivity(intent);

            });
/*
            Bundle args = new Bundle();
            String personJsonString = Utils.getGsonParser().toJson(person);
            args.putString(ProductDetailActivity.PRODUCT_EXTRA_KEY, personJsonString);*/
        }

    }

}
